const header = document.getElementById('header')
const game = document.getElementById("jogo");
const showVictory = document.createElement("div");

let currentCol = 0;
let currentCel = 0;
let currentCelElement = 0;
let currentPlayer = "blue";
let countPlays = 0;
let winBlue = 0
let winRed = 0


function tabela() {
  countPlays = 0
  game.innerHTML = ""
  for (let c = 10; c <= 70; c += 10) {
    game.insertAdjacentHTML("beforeend", `<div class="col" id="${c}"></div>`);
    for (let l = 6; l >= 1; l--) {
      document.getElementById(c).insertAdjacentHTML(
          "beforeend",
          `<div class= "square" id="${c + l}"></div>`
        );
    }
    document.getElementById(c).addEventListener("click", addPlayer);
  }
  renderCurrentPlayer()
}

tabela();

// ADICIONA DISCO
function addPlayer(e) {
    header.innerHTML = ""
    currentCol = e.currentTarget;
    currentCel = Number(currentCol.lastElementChild.id);
    currentCelElement = (currentCol.lastElementChild)
    for (currentCel; currentCel < currentCel + 5; currentCel++) {
      let cel = document.getElementById(currentCel);
      if (cel.lastChild === null) {
        renderPlayer(cel);
        break;
      }
    }
  
}

function renderPlayer(cel) {
  cel.insertAdjacentHTML("beforeend", `<div class= "disc ${currentPlayer}"></div>`);
  countPlays++
    if(countPlays > 6){
      verifyLine();
      verifyColunm();
      verifyDiagonalLeft();
      verifyDiagonalRigth();
      verifyDraw()
  }
  switchPlayer();
}

function renderCurrentPlayer(){
  header.innerHTML = ""
  header.insertAdjacentHTML("beforeend", `<p>Agora é a vez do player ${currentPlayer}!</p><div class= "${currentPlayer} currentPlayer"></div> <p>Blue ${winBlue} x Red ${winRed}</p>`)
}

function switchPlayer() {
  if (currentPlayer === "blue") {
    currentPlayer = "red";
  } else {
    currentPlayer = "blue";
  }
  renderCurrentPlayer()
}


//VALIDAÇÃO DA JOGADA

function verifyLine(){
  let countWin = 1
  let currentBackCel = document.getElementById(currentCel - 10)
  let currentNextCel = document.getElementById(currentCel + 10)
  
  for(let a = 1; a < 4; a++){ 
    if(currentBackCel !== null && currentBackCel.lastChild !== null && currentBackCel.lastChild.classList.contains(currentPlayer)){
      countWin++
      currentBackCel = document.getElementById(Number(currentBackCel.id) - 10)
    }
  }
  for(let b = 1; b < 4; b++){
      if(currentNextCel !== null && currentNextCel.lastChild !== null && currentNextCel.lastChild.classList.contains(currentPlayer)){
        countWin++
        currentNextCel = document.getElementById(Number(currentNextCel.id) + 10)
    }     
  }
 
  if(countWin >= 4){    
    win() 
   
  }
}


  function verifyColunm(){
  let countWin = 1
  let currentDownCel = document.getElementById(currentCel-1)
  
    for(let i = 1; i <= 3; i++){
      if(currentDownCel !== null && currentDownCel.lastChild !== null && currentDownCel.lastChild.classList.contains(currentPlayer)){
        countWin++
        currentDownCel = document.getElementById(Number(currentDownCel.id)-1)
        }
      if(countWin === 4){       
        win()      
        
      }      
    } 
  }



  function verifyDiagonalLeft(){
    let countWin = 1
    let currentBackCel = document.getElementById(currentCel - 10 - 1)
    let currentNextCel = document.getElementById(currentCel + 10 + 1)
    
    for(let a = 1; a < 4; a++){ 
      if(currentBackCel !== null && currentBackCel.lastChild !== null && currentBackCel.lastChild.classList.contains(currentPlayer)){
        countWin++
        currentBackCel = document.getElementById(Number(currentBackCel.id) - 10 - 1)
      }
    }
  
    for(let b = 1; b < 4; b++){
        if(currentNextCel !== null && currentNextCel.lastChild !== null && currentNextCel.lastChild.classList.contains(currentPlayer)){
          countWin++
          currentNextCel = document.getElementById(Number(currentNextCel.id) + 10 + 1)
  
      }     
    }
   
    if(countWin === 4){     
      win()     
      
    }
  }

  function verifyDiagonalRigth(){
    let countWin = 1
    let currentBackCel = document.getElementById(currentCel - 10 + 1)
    let currentNextCel = document.getElementById(currentCel + 10 - 1)
    
    for(let a = 1; a < 4; a++){ 
      if(currentBackCel !== null && currentBackCel.lastChild !== null && currentBackCel.lastChild.classList.contains(currentPlayer)){
        countWin++
        currentBackCel = document.getElementById(Number(currentBackCel.id) - 10 + 1)
      }
    }
  
    for(let b = 1; b < 4; b++){
        if(currentNextCel !== null && currentNextCel.lastChild !== null && currentNextCel.lastChild.classList.contains(currentPlayer)){
          countWin++
          currentNextCel = document.getElementById(Number(currentNextCel.id) + 10 - 1)
      }     
    }
    
    if(countWin === 4){      
      win()      
      
    }
  }

  function verifyDraw(){
    if(countPlays === 42){
      displayDraw()
      setTimeout(function (){
        tabela()
      }, 4000)
    }
  }

  function win(){
    if(currentPlayer === "blue"){
      winBlue++
    }
    else{
      winRed++
    }
    console.log(winBlue)
    displayWin(currentPlayer)
    setTimeout(function (){
      tabela()
    }, 3000)
  }

  function displayWin (actualPlayer){
    game.innerHTML = ""
    let winner = actualPlayer.toUpperCase()
    
    showVictory.innerText = winner + " Venceu!"   
    showVictory.classList.add("Winner")  
    
    showVictory.innerText = winner + " Venceu!"   
    showVictory.classList.add("Winner")  
    
    game.appendChild(showVictory)
  }

  function displayDraw (){
    game.innerHTML = ""  
    
    showVictory.innerText = "Empatou!"    
    showVictory.classList.add("Winner") 
    
    game.appendChild(showVictory)
  }  
